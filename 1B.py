"""
@author: Karolina Majka
Pomoc w postaci Pana wykładów wrzuconych na e portal oraz przede wszystkim 
https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 
"""
""" Definicja funkcji, która przyjmuje jeden argument L (listę liczb)."""

def filtruj_wieksze_od_poprzednich(L):
    """  Sprawdzenie, czy lista L jest pusta.  Jeśli L jest pusta, funkcja zwraca pustą listę. """
    if not L:
        return []
    """Inicjalizacja listy wynikowej wynik z pierwszym elementem L. 
    Pierwszy element zawsze trafia do wyniku, ponieważ nie ma poprzednich liczb do porównania. """
    wynik = [L[0]] 
    """ Ustawienie początkowej maksymalnej wartości na pierwszy element listy L."""
    maks = L[0]    
    """ Pętla for, która sprawdza wszystkie elementy listy L, zaczynając od drugiego elementu 
    (L[1:] oznacza podlistę zaczynającą się od indeksu 1 do końca listy). """
    for liczba in L[1:]:
        """  Sprawdzenie, czy bieżący element liczba jest większy od maksymalnej dotychczasowej wartości maks"""
        if liczba > maks:
            """ Jeśli warunek if jest spełniony, bieżąca liczba zostaje dodana do listy wynikowej wynik."""
            wynik.append(liczba)
            """ Aktualizacja maksymalnej wartości maks na bieżącą liczbę """
            maks = liczba
    """Funkcja zwraca listę wynik, zawierającą wszystkie liczby z L, które były 
    większe od wszystkich poprzednich liczb. """
    return wynik
def testuj_filtruj_wieksze_od_poprzednich():
    """Sprawdzenie, czy wynik funkcji dla listy [1, 13, 4, 13, -10, 21, 9] to [1, 13, 21]. 
    Jeśli nie, wyświetli komunikat "Test 1 nie przeszedł" """   
    assert filtruj_wieksze_od_poprzednich([1, 13, 4, 13, -10, 21, 9]) == [1, 13, 21], "Test 1 nie przeszedł"
    """Sprawdzenie, czy wynik funkcji dla pustej listy [] to również pusta lista []. 
    Jeśli nie, wyświetli komunikat "Test 2 nie przeszedł". """
    assert filtruj_wieksze_od_poprzednich([]) == [], "Test 2 nie przeszedł" 
    """ Sprawdzenie, czy wynik funkcji dla listy [2, 2, 2, 2] to [2]. 
    Jeśli nie, wyświetli komunikat "Test 3 nie przeszedł".""" 
    assert filtruj_wieksze_od_poprzednich([2, 2, 2, 2]) == [2], "Test 3 nie przeszedł" 
    
    """Jeśli wszystkie asercje są prawdziwe, wyświetli komunikat "Wszystkie testy przeszły pomyślnie!"."""
    print("Wszystkie testy przeszły pomyślnie!")

""" Wywołanie  funkcji testującej """
testuj_filtruj_wieksze_od_poprzednich()
